package tima.model;

public class RequestBodyAPI {
    private String request_id;
    private String mobile;
    private String national_id;
    private String loan_id;
    private String model_version;


    private int salary_amount_System;
    private int salary_amount_contract;
    private int salary_month_first;
    private int salary_month_second;
    private int salary_month_third;
    private int amount_money_credit;
    private int amount_money_spent;
    private int credit_duration;
    private int interest_rate;

    public int getSalary_amount_System() {
        return salary_amount_System;
    }

    public void setSalary_amount_System(int salary_amount_System) {
        this.salary_amount_System = salary_amount_System;
    }

    public int getSalary_amount_contract() {
        return salary_amount_contract;
    }

    public void setSalary_amount_contract(int salary_amount_contract) {
        this.salary_amount_contract = salary_amount_contract;
    }

    public int getSalary_month_first() {
        return salary_month_first;
    }

    public void setSalary_month_first(int salary_month_first) {
        this.salary_month_first = salary_month_first;
    }

    public int getSalary_month_second() {
        return salary_month_second;
    }

    public void setSalary_month_second(int salary_month_second) {
        this.salary_month_second = salary_month_second;
    }

    public int getSalary_month_third() {
        return salary_month_third;
    }

    public void setSalary_month_third(int salary_month_third) {
        this.salary_month_third = salary_month_third;
    }

    public int getAmount_money_credit() {
        return amount_money_credit;
    }

    public void setAmount_money_credit(int amount_money_credit) {
        this.amount_money_credit = amount_money_credit;
    }

    public int getAmount_money_spent() {
        return amount_money_spent;
    }

    public void setAmount_money_spent(int amount_money_spent) {
        this.amount_money_spent = amount_money_spent;
    }

    public int getCredit_duration() {
        return credit_duration;
    }

    public void setCredit_duration(int credit_duration) {
        this.credit_duration = credit_duration;
    }

    public int getInterest_rate() {
        return interest_rate;
    }

    public void setInterest_rate(int interest_rate) {
        this.interest_rate = interest_rate;
    }


    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNational_id() {
        return national_id;
    }

    public void setNational_id(String national_id) {
        this.national_id = national_id;
    }

    public String getLoan_id() {
        return loan_id;
    }

    public void setLoan_id(String loan_id) {
        this.loan_id = loan_id;
    }

    public String getModel_version() {
        return model_version;
    }

    public void setModel_version(String model_version) {
        this.model_version = model_version;
    }
}
