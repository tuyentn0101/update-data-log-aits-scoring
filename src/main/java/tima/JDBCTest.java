package tima;

import com.google.gson.JsonObject;
import tima.connection.MySQLConnection;
import tima.connection.SQLServerConnection;
import tima.service.HttpPostJsonExample;

import java.util.HashMap;
import java.util.List;

public class JDBCTest {
    public static void main(String[] args) {
//        Logger logger = Logger.getLogger(tima.JDBCTest.class);
        MySQLConnection mySQLConnection = new MySQLConnection();
//        logger.log(1,);
        System.out.println("Connect mySQL");
        mySQLConnection.connectAndGetRequest();
        System.out.println("Get Request with status = 0");
        List<HashMap<String, String>> listOfRequest = mySQLConnection.listOfRequest;

        SQLServerConnection sqlServerConnection = new SQLServerConnection();
        System.out.println("Connect SQL server");
        sqlServerConnection.connectAndGetData(listOfRequest);
        System.out.println("Filling list ...");
        List<HashMap<String, String>> listFilledRequest = sqlServerConnection.listFilledRequest;

        HttpPostJsonExample httpPostJsonExample = new HttpPostJsonExample();

        for(HashMap<String, String> req : listFilledRequest) {
            JsonObject jsonRequest = new JsonObject();
            jsonRequest.addProperty("request_id", req.get("request_id"));
            jsonRequest.addProperty("mobile", req.get("mobile"));
            jsonRequest.addProperty("national_id", req.get("national_id"));
            jsonRequest.addProperty("loan_id", req.get("loan_id"));
            jsonRequest.addProperty("model_version", req.get("model_version"));
            JsonObject jsonSalary = new JsonObject();
            jsonSalary.addProperty("salary_amount_System", req.get("salary_amount_System"));
            jsonSalary.addProperty("salary_amount_contract", req.get("salary_amount_contract"));
            jsonSalary.addProperty("salary_month_first", req.get("salary_month_first"));
            jsonSalary.addProperty("salary_month_second", req.get("salary_month_second"));
            jsonSalary.addProperty("salary_month_third", req.get("salary_month_third"));
            jsonSalary.addProperty("amount_money_credit", req.get("amount_money_credit"));
            jsonSalary.addProperty("amount_money_spent", req.get("amount_money_spent"));
            jsonSalary.addProperty("credit_duration", req.get("credit_duration"));
            jsonSalary.addProperty("interest_rate", req.get("interest_rate"));
            jsonRequest.add("salary", jsonSalary);
            System.out.println(jsonRequest.toString());
            httpPostJsonExample.post(jsonRequest.toString());
        }
    }
}


