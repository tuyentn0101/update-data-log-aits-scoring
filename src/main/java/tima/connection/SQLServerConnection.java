package tima.connection;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;


public class SQLServerConnection {

    public List<HashMap<String, String>> listFilledRequest;

    public SQLServerConnection() {
        listFilledRequest = new ArrayList<>();
    }

    public void connectAndGetData(List<HashMap<String, String>> listRequest) {

        String connectionUrl = "jdbc:sqlserver://rdstima.cxhemkeqirbt.ap-southeast-1.rds.amazonaws.com:1433;databaseName=Mecash;user=BigData;password={LW/d9@m5q{3N}";

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            int count = 0;

            for (HashMap<String, String> req : listRequest) {
                HashMap<String, String> reqFilled = req;
//                String SQL =
//                        "select  TOP 1 a.CustomerCreditId, b.Phone, b.CardNumber, b.Salary as salary_amount_System, a.LoanID,  a.TotalMoney, a.LoanTime, a.TotalReturn, cast(a.CreateDate as Date) as CreateDate, isnull(c.SalaryLastMonth, '') as salary_month_first, isnull(c.Salary2rdMonthNearest, '') as salary_month_second, isnull(c.Salary3rdMonthNearest,'') as salary_month_third, isnull(c.SalaryContract,'') as salary_amount_contract, d.TotalMoney as amount_money_credit, d.TotalMoneyCurrent as amount_money_spent, d.LoanTime as credit_duration, d.Rate as interest_rate from tblLoanCredit a inner join tblCustomerCredit b on a.CustomerCreditId = b.CustomerId left join tblInformationToReceiveSalary c on a.ID = c.LoanCreditID inner join tblLoan d on a.LoanID = d.ID where b.Phone ='"
//                                + req.get("mobile") + "' and b.CardNumber = '" + req.get("national_id")
//                                + "' order by a.CreateDate desc;";


                String SQL = FileUtils.readFileToString(new File("config/SqlQuery"),"utf-8");
                SQL = String.format(SQL, req.get("mobile"), req.get("national_id"));


//                System.out.println(SQL);

                ResultSet rs = stmt.executeQuery(SQL);

                while (rs.next()) {
                    count++;
                    System.out.println("Fill " + (count) + " : " + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
                    reqFilled.put("loan_id", rs.getString("ID"));
                    reqFilled.put("model_version", "1.0");
                    reqFilled.put("salary_amount_System", rs.getString("salary_amount_System"));
                    reqFilled.put("salary_amount_contract", rs.getString("salary_amount_contract"));
                    reqFilled.put("salary_month_first", rs.getString("salary_month_first"));
                    reqFilled.put("salary_month_second", rs.getString("salary_month_second"));
                    reqFilled.put("salary_month_third", rs.getString("salary_month_third"));
                    reqFilled.put("amount_money_credit", rs.getString("amount_money_credit"));
                    reqFilled.put("amount_money_spent", rs.getString("amount_money_spent"));
                    reqFilled.put("credit_duration", rs.getString("credit_duration"));
                    reqFilled.put("interest_rate", rs.getString("interest_rate"));
                    this.listFilledRequest.add(reqFilled);
                }
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
